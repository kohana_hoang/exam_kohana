$(document).ready(function (e) {
    
    load_results();    
    function load_results(){         
        var bs_url = bUrlGet;     
        $.ajax({
            type:'POST',
            url: bs_url,          
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               $('#results').html(data);
            },
            error: function(data){
                alert('error')
            }
        });
    }
    
    $('#uploadImgFrm').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
       
        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,   
            async: false,   
            success:function(data){
                if(data == 'fail'){
                    $('.error').html('<span>Something wrong, try again!</span>').slideDown();
                }else{
                    $('.error').slideUp();
                    $('.close').trigger('click');
                    load_results();
                    $('#updateImgFrm').remove();
                }
            },
            error: function(data){
                alert('error')
            }
        });
      
    }));

    $("#submitFrm").on("click", function() {         
        $("#uploadImgFrm").submit();
    });         
   
});