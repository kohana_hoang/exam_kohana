<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title><?php echo $title; ?></title>
    <?php 
        foreach($styles as $file => $type)
        { 
            echo HTML::style($file, array('media' => $type)), ""; 
        } 
        foreach($scripts as $files => $type_script)
        { 
            echo HTML::script($type_script, array('src' => $type_script)), ""; 
        }    
    ?>    
	
</head>
<body>
    <?php echo $body; ?>
    <?php 
        foreach($scripts_footer as $files_f => $type_scripts)
        { 
            echo HTML::script($type_scripts, array('src' => $type_scripts)), ""; 
        }        
    ?>   
</body>
</html>