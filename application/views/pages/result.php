<?php if(!empty($results)){ ?>
<table>
    <thead>
        <tr>
            <th>Title</th>
            <th>Thumbnail</th>
            <th>File Name</th>
            <th>Date Added</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($results as $rs){ ?>
            <?php $id = $rs['id'] * 113; ?>
            <tr>
                <td><?php echo $rs['title']; ?></td>
                <td><img src="<?php echo URL::base().'uploads/'.$rs['thumbnail']; ?>" width="100"/></td>
                <td><?php echo $rs['filename']; ?></td>
                <td><?php echo $rs['date_added']; ?></td>
                <td><a id="editImg" href="#edit" rel="modal:open" class="button-edit" data-id="<?php echo $id; ?>" data-thumbs="<?php echo $rs['thumbnail']; ?>" data-thumb="<?php echo URL::base().'uploads/'.$rs['thumbnail']; ?>"  data-title="<?php echo $rs['title']; ?>" data-filename="<?php echo $rs['filename']; ?>" >Edit</a><a id="deleteImg" href="#" class="button-delete" data-id="<?php echo $id; ?>" data-thumbs="<?php echo $rs['thumbnail']; ?>">Delete</a></td>
            </tr>
        <?php } ?>
    </tbody>        
</table>

<div id="edit" class="modal" style="top: 0px!important;">
    <form id="updateImgFrm" action="<?php echo URL::base().'welcome/update_image'; ?>" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" />
        <input type="hidden" name="thumb" id="thumb" />
        <div class="form">  
            <label>Title</label>
            <input type="text" name="title" id="title" class="in-txt"/>
        </div>
        <div class="form">  
            <label>Image</label>
            <input type="file" name="image" />
        </div>
        <div class="form imgview">
            
        </div>
        <div class="form error"></div>
        <div class="form">
             <button type="button" id="updateFrm" class="button-success">Update</button>
        </div>
        
        <a href="#close-modal" rel="modal:close" class="close">x</a>
    </form>
</div>
<?php }else{ echo 'no record'; } ?>
<script type="text/javascript">
    $(document).ready(function (e) {
        
        $('body #deleteImg').on('click',function(){  
            var dtUrl  = "<?php echo URL::base().'welcome/deleteImg'; ?>";;                         
            var result = confirm("Are you sure you want to delete this item?");
            var rand = "<?php echo time(); ?>";   
            if (result) {
                var id = $(this).data('id');  
                var img_thumb = $(this).data('thumbs');
                $.ajax({
                    cache:false,
                    type:'GET',
                    url: dtUrl+"?rand=" + rand,
                    data:{'id':id, 'thumb':img_thumb},    
                    contentType: 'text',
                    processData: true,   
                    headers: { "cache-control": "no-cache" },     
                    success:function(data){                
                        if(data == 'fail'){
                            alert('some thing wrong, try again!')
                        }else{
                            alert('Delete Success!');
                            $('#updateImgFrm').remove();
                            load_results();
                        }                  
                    },
                    error: function(data){
                        alert('error');
                        
                    }
                });
            }
            
        });
        $('body #editImg').on('click',function(){            
            $('#id').val($(this).data('id'));  
            $('#title').val($(this).data('title'));
            $('#thumb').val($(this).data('thumbs'));
            $('.imgview').html('<img src="'+$(this).data('thumb')+'" width="100"/>');    
            $('.error').html('');
        });
    $('#updateImgFrm').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);  
        var rand = "<?php echo time(); ?>";             
        $.ajax({
            cache:false,
            type:'POST',
            url: $(this).attr('action')+"?rand=" + rand,
            data:formData,    
            contentType: false,
            processData: false,   
            headers: { "cache-control": "no-cache" },     
            success:function(data){                
                if(data == 'fail'){
                    $('.error').html('<span>Something wrong, try again!</span>').slideDown();
                }else{
                    $('.error').slideUp();
                    $('.close').trigger('click');
                    $('#updateImgFrm').remove();
                    load_results();
                }
          
            },
            error: function(data){
                alert('error');
                
            }
        });
        return false;
    }));
        function load_results(){         
            var bs_url = bUrlGet;             
            $.ajax({
                type:'POST',
                url: bs_url,          
                cache:false,
                contentType: false,
                processData: false,
                async: false,
                success:function(data){
                   $('#results').html(data);
                  
                },
                error: function(data){
                    alert('error')
                }
            });
        }
    $("#updateFrm").on("click", function() {
        $("#updateImgFrm").submit();
    });
    });
</script>