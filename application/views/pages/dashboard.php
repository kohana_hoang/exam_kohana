<div class="main">
    <h1>Manager Images</h1>
    <div class="add-img">
        <p><a href="#add" rel="modal:open" class="button-success">Add Image</a></p>
    </div>
    <div id="results">
        
    </div>
</div>

<div id="add" class="modal">
    <form id="uploadImgFrm" action="<?php echo URL::base().'welcome/add_image'; ?>" method="post" enctype="multipart/form-data">
        <div class="form">  
            <label>Title</label>
            <input type="text" name="title" class="in-txt"/>
        </div>
        <div class="form">  
            <label>Image</label>
            <input type="file" name="image" />
        </div>
        <div class="form error"></div>
        <div class="form">
             <button id="add" type="submit" id="submitFrm" class="button-success">Add</button>
        </div>
        
        <a href="#close-modal" rel="modal:close" class="close">x</a>
    </form>
</div>

<script type="text/javascript">
    var bUrlGet = "<?php echo URL::base().'welcome/getImage'; ?>";
</script>