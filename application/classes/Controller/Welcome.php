<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {
    
    public $template = 'template';
    
	public function action_index()
	{
		$this->response->body('hello, world!');
	}
    
    public function before()
    {
        parent::before();

        if($this->auto_render)
        {
            $this->template->styles = array();
            $this->template->scripts = array();
            $this->template->scripts_footer = array();
        }
    }

    public function after()
    {
        if($this->auto_render)
        {      
            $styles = array('media/css/style.css' => 'screen');
            $scripts = array('https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js','https://code.jquery.com/jquery-3.3.1.min.js');
            $scripts_footer = array('media/js/main.js');
            
            $this->template->styles  = array_reverse(array_merge($this->template->styles,$styles));
            $this->template->scripts = array_reverse(array_merge($this->template->scripts, $scripts));
            $this->template->scripts_footer = array_reverse(array_merge($this->template->scripts_footer, $scripts_footer));                        
        }     
        parent::after();
     }
    
    public function action_dashboard()
    {        
        $this->template->title = 'Kohana Exam!';
        $this->template->body = View::factory('pages/dashboard');//->set('haha','abc');
        //$this->auto_render = false;        
        //$results = DB::select()->from('images')->execute();      
        //$this->response->body(View::factory('pages/result')->set('results',$results));
    }
    
    public function action_getImage()
    {   
        $this->auto_render = false;        
        $results = DB::select()->from('images')->execute();      
        $this->response->body(View::factory('pages/result')->set('results',$results));
    }         
    
    public function action_add_image()
    {                                           
        $filename = null;
        $uploaded_filename = null;
        $imgarr = array();
     
        $this->auto_render = false;
        $title = $this->request->post('title');
        $msg = 'success';
      
        if($this->request->method() == Request::POST)
        {
            if(isset($_FILES['image']))
            {               
                $filename = $this->_save_image($_FILES['image']);                
                $uploaded_filename = $_FILES['image']['name'];
            }
        }              
        
        if(!$filename || $filename == 'FALSE' || $title == '' )
        {            
            $msg = 'fail';
            unlink(DOCROOT.'uploads/'.$filename);
        }else{
            
            $values = array( $title, 
                             $filename,
                             $uploaded_filename,
                             date('Y-m-d'));
            $query = DB::insert('images', array('title', 'thumbnail', 'filename', 'date_added'))->values($values); 
            $result = $query->execute();          
        }
                    
        echo $msg;
    }
    
    public function action_update_image()
    {                                           
        $filename = null;
        $uploaded_filename = null;
        $imgarr = array();
    
        $this->auto_render = false;
        $title = $this->request->post('title');
        $thumb = $this->request->post('thumb');
        $id    = $this->request->post('id');
        $id_query = $id / 113; 
        $msg = 'success';
        
        if($this->request->method() == Request::POST)
        {
            if(file_exists($_FILES['image']['tmp_name']))
            {               
                
                $filename = $this->_save_image($_FILES['image']);                
                $uploaded_filename = $_FILES['image']['name'];
                if($filename){
                    if(file_exists(DOCROOT.'uploads/'.$thumb)) {
                        unlink(DOCROOT.'uploads/'.$thumb);
                    }    
                }                
            }
            
        }              
        
        if($title == '' || $filename == 'FALSE')
        {                       
            $msg = 'fail';
            //unlink(DOCROOT.'uploads/'.$filename);
        }else{
            
            $updateArr = array( 'title'     => $title, 
                             'thumbnail' => $filename,
                             'filename'  =>$uploaded_filename,
                           );
             
            if(!$filename){
                $query = DB::update('images')->set(array('title' => $title))->where('id', '=', $id_query);
            }else{                
                $query = DB::update('images')->set($updateArr)->where('id', '=', $id_query);  
                                     
            }
            
            $result = $query->execute();          
        }
                    
        echo $msg;
    }        
    
    protected function _save_image($image)
    {
      // check is uploaded file a valid image file
        if( !Upload::not_empty($image) OR
            !Upload::valid($image) OR
            !Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return 'FALSE';
        }
        
        $upload_dir = DOCROOT.'uploads/';
        
      // save image to 'uploads' foler in docroot
        if($file = Upload::save($image, NULL, $upload_dir))
        {
           // randomly generate 20 alphanumeric characters as file name
            $filename = strtolower(Text::random('alnum', 20)).'.jpg';
            
          // resize image file to 210x210
            Image::factory($file)
                  ->resize(210, 210, Image::NONE)
                  ->save($upload_dir.$filename);
            
           // remove image file with old name
            unlink($file);
            return $filename;
        }
        
        return FALSE;
    }
    public function action_deleteImg()
    { 
        $this->auto_render = false;
        $thumb  = $_GET['thumb'];
        $ids    = $_GET['id'];
        $id = $ids / 113; 
        
        $query = DB::delete('images')->where('id', '=', $id);
        $result = $query->execute();  
        if($result){
            if(file_exists(DOCROOT.'uploads/'.$thumb)) {
                unlink(DOCROOT.'uploads/'.$thumb);
            }    
            $msg = 'success';
        }else{
            $msg = 'fail';
        }
    }
} // End Welcome
